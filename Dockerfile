FROM node

WORKDIR /app
COPY . .

EXPOSE 7060

CMD ["npm", "install"]
CMD ["node", "app.js"]

 