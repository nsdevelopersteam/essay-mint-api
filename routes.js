
var Joi                     = require('joi');

const OrderController     = require('./Controllers/OrderController');
const AdminController     = require("./Controllers/AdminController");
const TransferController  = require("./Controllers/TransfersController");

module.exports = [
    {
        method: 'POST',
        path: '/admin/add',
        config: {
            description: 'New Admin',
            notes: 'This route add a new admin to the system',
            tags: ['api'], 
            auth: 'superadmin',
            validate: {
                payload: {
                    email: Joi.string().email().required().description("email id witch is unique to every an admin and its used for authentication purposes"),
                    name: Joi.string().required().description("name of the admin"),
                }
            }
        },
        handler: AdminController.addAdmin
    },
    {
        method: 'POST',
        path: '/admin/setupstats',
        config: {
            description: 'Setup Statistics',
            notes: 'Setup the statistics for the new system',
            tags: ['api'], 
            auth: 'superadmin',
        },
        handler: AdminController.setupStatistics
    },
    {
        method: 'POST',
        path: '/admin/auth',
        config: {
            description: 'Auth Admin',
            notes: 'Route used to authenticate admins',
            tags: ['api'], 
            auth: false,
            validate: {
                payload: {
                    email: Joi.string().email().required().description("admin's email"),
                    password: Joi.string().required().description("admin's password"),
                }
            }
        },
        handler: AdminController.authAdmin
    },

    {
        method: 'PATCH',
        path: '/admin/changepassword',
        config: {
            description: 'Change Password Admin',
            notes: "Route used to change admin's password",
            tags: ['api'], 
            auth: false,
            validate: {
                payload: {
                    email: Joi.string().email().required().description("admin's email"),
                    currentPassword: Joi.string().required().description("admin's current password"),
                    newPassword: Joi.string().required().description("admin's new password"),
                }
            }
        },
        handler: AdminController.changePassword
    },

    {
        method: 'POST',
        path: '/order/process',
        config: {
            description: 'New Order',
            notes: 'This route process a new order after a sucessfull payment',
            tags: ['api'], 
            auth: false,
            validate: {
                payload: {
                    type: Joi.string().required().description("type of the essay that the customer choose"),
                    title: Joi.string().required().description("title inserted by the customer"),
                    numberOfPages: Joi.number().required().description("number of the pages inserted by the customer"),
                    spacing: Joi.string().required().description("spacing chosen by the customer"), 
                    academicLevel: Joi.string().required().description("academic level chosen by the customer"),
                    urgency: Joi.string().required().description("urgency chosen by the customer"),
                    notes: Joi.optional().description("notes inserted by the customer"),
                    sourcesList: Joi.array().description("source list of the pdfs that are on media"),
                    total: Joi.number().required().description("total paid by the customer"),
                    fee: Joi.number().required().description("fee applied on the order"),
                    paymentConfirmationId: Joi.string().required().description("payment Id to confirm that the payment was performed"),
                    customerName: Joi.string().required().description("customer's name"),
                    customerEmail: Joi.string().email().description("customer's email"),
                    style: Joi.string().required().description("style of the paper"),
                    languageStyle: Joi.string().required().description("language preference US/UK"),
                    numberOfSources: Joi.number().required().description("number of references"),
                }
            }
        },
        handler: OrderController.addOrder
    },

    {
        method: 'POST',
        path: '/order/pay',
        config: {
            description: 'Process payment',
            notes: 'Process the payment of a company',
            tags: ['api'], 
            auth: false,
            validate: {
                payload: {
                    cardn: Joi.string().required().description("card number to be charged"),
                    expmonth: Joi.number().required().description("exp month of the card"),
                    expyear: Joi.number().required().description("exp year of the card"),
                    cvv: Joi.string().required().description("CVV of the card"),
                    amount: Joi.number().required().description("amount to be charged"),
                    description: Joi.string().required().description("description of the paymnet for stripe"),
                }
            }
        },
        handler: OrderController.processPayment
    },
    {
        method: 'GET',
        path: '/order/getall',
        config: {
            description: 'Get All Orders',
            notes: 'Get all the orders processed',
            tags: ['api'], 
            auth: 'admin',
        },
        handler: OrderController.retrieveAllOrders
    },
    {
        method: 'PATCH',
        path: '/order/changestatus',
        config: {
            description: 'Change Orders Status',
            notes: 'Allow admin to change the status of a order',
            tags: ['api'], 
            auth: 'admin',
            validate: {
                payload: {
                    publicId: Joi.string().required().description("Public Id of the Order"),
                    newStatus: Joi.string().required().description("new status of the order"),
                }
            }
        },
        handler: OrderController.changeOrderStatus
    },
    {
        method: 'GET',
        path: '/admin/getstats',
        config: {
            description: 'Get Statistics',
            notes: 'Get the statistics that indicates all the system is doing',
            tags: ['api'], 
            auth: 'admin',
        },
        handler: AdminController.getStatistics
    },

    {
        method: 'GET',
        path: '/order/details/{publicId}',
        config: {
            description: 'Get Order Details',
            notes: 'Get the details of an order',
            tags: ['api'], 
            auth: 'admin',
        },
        handler: OrderController.GetOrderDetails
    },

    {
        method: 'POST',
        path: '/transfer/cachout',
        config: {
            description: 'Cashout To Admin',
            notes: 'Cashout the undeposited money to admin and deposit to paypal',
            tags: ['api'], 
            auth: 'admin',
        },
        handler: TransferController.processTransfer
    },

    {
        method: 'GET',
        path: '/guest/countvisit',
        config: {
            description: 'Visitor Counter',
            notes: 'Increment the visitor counter when someone access the page',
            tags: ['api'], 
            auth: false,
        },
        handler: AdminController.CountVisit
    },
]
