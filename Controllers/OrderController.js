
const boom = require('boom');
const randomstring = require("randomstring");
const utils = require("../utils")
const randomInt = require('random-int');

const OrderModel = require("../Models/OrderModel");
const PaymentHistoryModel = require("../Models/PaymentModel");
const StatisticsModel = require("../Models/StatisticsModel")

const configuration = require('../config');
const mailer = require("../mailer");

module.exports = {

    addOrder : async (request, h) => {
        const promise = new Promise((resolve, reject) => {
            try {
                PaymentHistoryModel.findOne({$and:[{'paymentId': request.payload.paymentConfirmationId }, {'isUsed': false}]}, function (err, paymentRecord) {
                    if(err) {
                        console.log(err)
                        reject(boom.internal("Your payment was processed but your order was not recorded, please contact the administrator as soon as possible please.")) 
                    }
                    else {
                        if(paymentRecord === null) {
                            reject(boom.badRequest("Payment record not found, you need to pay before sending your order"))
                        }
                        else {
                            var publicIdParam = randomstring.generate(15);
                            var orderId = randomInt(100000, 999999);

                            var newOrder = new OrderModel({ 
                                publicId: publicIdParam,
                                orderId: orderId, // a 6 digits number that represents the order
                                type: request.payload.type, // Type of essay
                                title: request.payload.title, // title of the essay
                                numberOfPages: request.payload.numberOfPages,
                                spacing: request.payload.spacing,
                                academicLevel: request.payload.academicLevel,
                                urgency: request.payload.urgency,
                                notes: request.payload.notes,
                                sourcesList: request.payload.sourcesList,
                                total: request.payload.total,
                                fee: request.payload.fee,
                                paymentConfirmationId: request.payload.paymentConfirmationId,
                                style: request.payload.style,
                                languageStyle: request.payload.languageStyle,
                                numberOfSources: request.payload.numberOfSources,
                                customer: {
                                    name: request.payload.customerName,
                                    email: request.payload.customerEmail,
                                }
                            });
                    
                            newOrder.save(function (err, saved) {
                                if (err) { 
                                    console.log(err)
                                    reject(boom.internal()) 
                                }
                                else {     
                                 
                                    paymentRecord.isUsed = true
                                    paymentRecord.save();

                                    var dataToMail = {
                                        amount: "$" + request.payload.total.toFixed(2),
                                        orderId: orderId
                                    }   
   
                                    mailer.send("neworder", request.payload.customerEmail, "Essay Mint - Order " + orderId, dataToMail)     
                                    resolve(new Object({
                                        statusCode: 200,
                                        error : null,
                                        message: "success",
                                        orderId: saved.orderId
                                    })) 

                                    // TODO change public Id for production
                                    StatisticsModel.findOne({"publicId": "deD4EfNN2TtqADe"}, function(err, statsInfo) {
                                        statsInfo.undepositedAmount = statsInfo.undepositedAmount + request.payload.total
                                        statsInfo.totalEarned = statsInfo.totalEarned + request.payload.total
                                        statsInfo.totalNumberOfOrders = statsInfo.totalNumberOfOrders + 1

                                        statsInfo.save();
                                    })
                                }
                            })
                        }
                    }
                })
                
            } catch (error) {
                console.log(error)
                reject(boom.internal()) 
            }
        });

        return promise;
    },

    processPayment: async (request, h) => {
        const promise = new Promise((resolve, reject) => {
            utils.ChargeCard(request.payload.cardn, request.payload.expmonth, request.payload.expyear, request.payload.cvv, request.payload.amount, "USD", request.payload.description) 
            .then((successChange) => {
                var newPayment = new PaymentHistoryModel({ 
                    publicId: randomstring.generate(15),
                    paymentInfo: {
                        cardNumberLast4: request.payload.cardn.substring(request.payload.cardn.length - 4, request.payload.cardn.length),
                        cardExpDate: request.payload.expmonth + "/" + request.payload.expyear,
                    },
                    total: request.payload.amount,
                    currency: "USD",
                    paymentId: successChange.id
                });

                newPayment.save(function (err, savedPayment) {
                    resolve(new Object({
                        statusCode: 200,
                        error : null,
                        message: "success",
                        paymentId: successChange.id
                    })) 
                })
            })
            .catch(errorPayment => {
                console.log(errorPayment)
                reject(boom.badRequest(errorPayment.message)) 
            })
            
        })
        return promise
    },

    retrieveAllOrders: async (request, h) => {
        const promise = new Promise((resolve, reject) => {
            try {
                OrderModel.find({}, function(err, records) {
                    if(err) {
                        console.log(err)
                        reject(boom.internal())
                    }
                    else {
                        var responseJson = {
                            statusCode: 200,
                            error: null,
                            message: "success",
                            records: records
                        }
    
                        resolve(responseJson)
                    }
                })
            } catch (error) {
                console.log(error)
                reject(boom.internal())
            }
            
        })

        return promise
    },

    changeOrderStatus: async (request, h) => {
        const promise = new Promise((resolve, reject) => {
            OrderModel.findOneAndUpdate({"publicId": request.payload.publicId}, {$set: {"status": request.payload.newStatus}}, {returnNewDocument: true}, function(err, orderInfo) {
                if(orderInfo === null) {
                    reject(boom.notFound("Order not found. logout, login and try again"))
                }
                else {
                    if(request.payload.newStatus === "ready") {
                        var dataToMail = {
    
                        }   
    
                        mailer.send("orderready", orderInfo.customerEmail, "Essay Mint - Order " + orderInfo.orderId + " Ready for Download", dataToMail)     
                    }
                    resolve(new Object({
                        statusCode: 200,
                        error : null,
                        message: "success",
                    }))    
                }
            })
        })

        return promise
    },

    GetOrderDetails: async (request, h) => {
        const promise = new Promise((resolve, reject) => {
            OrderModel.findOne({publicId: request.params.publicId}, function(err, order) {
                if(err) {
                    reject(boom.internal())
                }
                else {
                    if(order === null || order === undefined) {
                        reject(boom.notFound("Order not found, logout and log back in again"))
                    }
                    else {
                        resolve(new Object({
                            statusCode: 200,
                            error : null,
                            message: "success",
                            order: order
                        })) 
                    }
                }
            })  
        })
        
        return promise
    }
}