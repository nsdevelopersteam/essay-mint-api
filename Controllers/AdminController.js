const mailer = require("../mailer")
const randomstring = require("randomstring");
const bcrypt = require('bcrypt-nodejs');
const boom = require("boom")
const jsonwebtoken = require('jsonwebtoken');

const AdminModel = require("../Models/AdminModel");
const StatisticsModel = require("../Models/StatisticsModel")

const configuration = require('../config');

module.exports = {

    addAdmin : async (request, h) => {
        //TODO - send email to company
        const promise = new Promise((resolve, reject) => {
            try {
                var publicId = randomstring.generate(15);
                var tmpPassword = randomstring.generate(6);
         
                var newAdmin = new AdminModel({ 
                    name: request.payload.name,
                    email: request.payload.email,
                    password: bcrypt.hashSync(tmpPassword),
                    publicId: publicId,
                });
        
                newAdmin.save(function (err, saved) {
                    if (err) { 
                        reject(boom.badRequest("There is a account with this name already.")) 
                    }
                    else {    
                        var dataToMail = {
                            password: tmpPassword
                        }      

                        mailer.send("newadmin", request.payload.email, "New Account", dataToMail)
                        .then(emailSucess => {
                            resolve(new Object({
                                statusCode: 200,
                                error : null,
                                message: "success",
                            })) 
                        })
                        .catch(error => {
                            console.log(error)
                            resolve(new Object({
                                statusCode: 200,
                                error : null,
                                message: "email not sent",
                            })) 
                        })
                        
                    }
                })

            } catch (error) {
                console.log(error)
                reject(boom.internal()) 
            }
        });

        return promise;
    },

    authAdmin: async (request, h) => {
        const promise = new Promise((resolve, reject) => {
            try {
                AdminModel.findOne({ 'email': request.payload.email.toLowerCase() }, function (err, person) {
                    if (err) {
                        reject(boom.internal()) 
                    }
                    else {
                        if(person != null) {
                            if(person.firstTime) {
                                reject(boom.badRequest("change password"))
                            }
                            else {
                                try {
                                    var plainPassFromUser = request.payload.password;
                                    var hashFromDB = person.password;
                                    
                                    bcrypt.compare(plainPassFromUser, hashFromDB, function(err, matches) {
                                        if (err) {
                                            reject(boom.internal()) 
                                        }
                                        else {
                                            if(matches) {
                                                var LoggedPerson = {
                                                    email: person.email,
                                                    password: person.password,
                                                    publicID: person.publicId,
                                                }
                            
                                                var token = jsonwebtoken.sign(LoggedPerson, configuration.superadmin.secret, { expiresIn: 3.154e+7 }); // 60*5 minutes
                                                person.authToken = 'Bearer ' + token;
                                                person.save();
                            
                                                resolve(new Object({
                                                    statusCode: 200,
                                                    error: null,
                                                    message: "success",
                                                    authToken: token,
                                                })) 
                                            }
                                            else {
                                                reject(boom.notFound("Your credentials does not match, please try again"));
                                            }
                                        }
                                    });
    
                                } catch (error) {
                                    reject(boom.internal()) 
                                }
                            }
                        }
                        else {
                            reject(boom.notFound("Your credentials does not match, please try again"));
                        }
                        
                    }  
                })
            } catch (error) {
                console.log(error)
                reject(boom.internal()) 
            }
        });

        return promise;
    },

    changePassword: async (request, h) => {
        const promise = new Promise((resolve, reject) => {
            try {
                AdminModel.findOne({ 'email': request.payload.email.toLowerCase() }, function (err, person) {
                    if (err) {
                        reject(boom.internal()) 
                    }
                    else {
                        var plainPassFromUser = request.payload.currentPassword;
                        var hashFromDB = person.password;

                        bcrypt.compare(plainPassFromUser, hashFromDB, function(err, matches) {
                            if (err) {
                                reject(boom.internal()) 
                            }
                            else {
                                if(matches) {
                                    person.password = bcrypt.hashSync(request.payload.newPassword)
                                    person.firstTime = false;
                                    person.save();

                                    resolve(new Object({
                                        statusCode: 200,
                                        error: null,
                                        message: "success",
                                    })) 
                                }
                                else {
                                    reject(boom.badRequest("The current password does not match"))
                                }
                            }
                        })
                    }
                })   
            } catch (error) {
                reject(boom.internal())
            }
        })

        return promise
    },

    findAdminByEmail: async (emailParam) => {
        const promise = new Promise((resolve, reject) => {
            AdminModel.findOne({ 'email': emailParam }, function (err, person) {
                if(err) {
                    reject(err)
                }
                else {
                    if(person === null) {
                        reject(null)
                    }
                    else {
                        resolve(person)
                    }
                }
            })
        })
        return promise
    },

    setupStatistics: async (request, h) => {
        const promise = new Promise((resolve, reject) => {
            try {
                var publicId = randomstring.generate(15);
            
                var newStats = new StatisticsModel({ 
                    publicId: publicId,
                });

                newStats.save(function (err, saved) {
                    resolve(new Object({
                        statusCode: 200,
                        error : null,
                        message: "success",
                    })) 
                })
            } catch (error) {
                reject(boom.internal())
            }
        })
        
        return promise
    },

    getStatistics: async (request, h) => {
        // TODO change the statistic Id
        const promise = new Promise((resolve, reject) => {
            StatisticsModel.findOne({ 'publicId': "deD4EfNN2TtqADe" }, function (err, infoStats) {
                var responseJson = {
                    statusCode: 200,
                    error: null,
                    message: 'success',
                    stats: infoStats
                }
                resolve(responseJson)
            });  
        })

        return promise
    },

    ResetDailyCounter: async () => {
        // TODO change the statistic Id
        const promise = new Promise((resolve, reject) => {
            StatisticsModel.findOne({ 'publicId': "deD4EfNN2TtqADe" }, function (err, infoStats) {
                infoStats.numberOfVisits = 0;
                infoStats.save();

                var responseJson = {
                    statusCode: 200,
                    error: null,
                    message: 'success',
                }
                resolve(responseJson)
            });  
        })

        return promise
    },

    CountVisit: async (request, h) => {
        // TODO change the statistic Id
        const promise = new Promise((resolve, reject) => {
            StatisticsModel.findOne({ 'publicId': "deD4EfNN2TtqADe" }, function (err, infoStats) {
                infoStats.numberOfVisits = parseInt(infoStats.numberOfVisits) + 1;
                infoStats.save();

                var responseJson = {
                    statusCode: 200,
                    error: null,
                    message: 'success',
                }
                resolve(responseJson)
            });  
        })

        return promise
    }
}