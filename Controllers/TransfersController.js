
const boom = require('boom');
const randomstring = require("randomstring");
const utils = require("../utils")
const randomInt = require('random-int');

const TransferModel = require("../Models/TransfersModel")
const StatisticsModel = require("../Models/StatisticsModel")

const configuration = require('../config');
const mailer = require("../mailer");

module.exports = {

    processTransfer: async (request, h) => {
        const promise = new Promise((resolve, reject) => {

            // TODO - change public Id for production on setup
            StatisticsModel.findOne({ 'publicId': "deD4EfNN2TtqADe" }, function (err, infoStats) {
                var sender_batch_id = Math.random().toString(36).substring(9);
                var lastTransferDate = Date.now
                var description = "Transference from " + infoStats.lastTransferDate + " to " + lastTransferDate

                utils.TransferMoney(parseFloat(infoStats.undepositedAmount).toFixed(2), description, sender_batch_id) 
                .then((successTransf) => {
                    var newTransfer = new TransferModel({ 
                        senderBatchId: sender_batch_id,
                        total: parseFloat(infoStats.undepositedAmount).toFixed(2),
                        transferId: successTransf.id
                    });

                    newTransfer.save(function (err, savedTransfer) {
                        infoStats.undepositedAmount = 0;
                        infoStats.lastTransferDate = lastTransferDate
                        infoStats.save();

                        //send email to me

                        resolve(new Object({
                            statusCode: 200,
                            error : null,
                            message: "success",
                        })) 
                    })
                })
                .catch(errorPayment => {
                    console.log(errorPayment)
                    reject(boom.badRequest(errorPayment.message)) 
                })
            });  
            
        })
        return promise
    },

}