const Q = require('q');
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const config = require("./config")

var stripe = require("stripe")(config.payment.testCred);
var paypal = require('paypal-rest-sdk');
paypal.configure(config.paypal);

const utils = {
    ChargeCard (cardNParam, cardMonthParam, cardExpYearParam, cardCvvParam, amountParam, currencyParam, descriptionParam) {
        var deferred = Q.defer();

        stripe.tokens.create({
        card: {
            "number": cardNParam,
            "exp_month": cardMonthParam,
            "exp_year": cardExpYearParam,
            "cvc": cardCvvParam
        }
        }, function(err, token) {
            if(err) {
                deferred.reject(err) 
            }
            else {
                stripe.charges.create({
                    amount: amountParam,
                    currency: currencyParam,
                    source: token.id,
                    description: descriptionParam
                  }, function(err, charge) {
                        if(!err) {
                            deferred.resolve(charge)
                        }
                        else {
                            deferred.reject(err) 
                        }
                  });
            }
        });
        
        return deferred.promise
    },

    SendEmail () {
        var transporter = nodemailer.createTransport(smtpTransport({
            service: 'gmail',
            auth: {
                user: config.email.testing.id,
                pass: config.email.testing.password
            }
        }));
        
        var mailOptions = {
            from: config.email.testing.id,
            to: emailToParam.toLowerCase(),
            subject: subjectParam,
            html: msgHTMLParam
        };                  
    
        transporter.sendMail(mailOptions, function(error, info){
        })  
    },

    TransferMoney (amountParam, descriptionParam, senderBatchParam) {
        var deferred = Q.defer();
        
        var create_payout_json = {
            "sender_batch_header": {
                "sender_batch_id": senderBatchParam,
                "email_subject": "You have a payment"
            },
            "items": [
                {
                    "recipient_type": "EMAIL",
                    "amount": {
                        "value": amountParam,
                        "currency": "USD"
                    },
                    "receiver": "nossappscmp-buyer@gmail.com",
                    "note": descriptionParam,
                    "sender_item_id": "Essay Mint Profit"
                }
            ]
        };

        var sync_mode = 'true';

        paypal.payout.create(create_payout_json, sync_mode, function (error, payout) {
            if (error) {
                console.log(error.response);
                deferred.reject(error)
            } else {
                console.log("Create Single Payout Response");
                console.log(payout);
                deferred.resolve(payout)
            }
        });

        return deferred.promise
    }
}

module.exports = utils
