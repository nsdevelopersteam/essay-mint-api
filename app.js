'use strict';

const Hapi = require('hapi');
const Routes = require('./routes.js');
const configuration = require('./config.js');
const cron = require('node-schedule');
const jsonwebtoken = require('jsonwebtoken');
const Q = require('q');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');
const Vision = require('vision');
const database = require("./Database/db");
const AdminController = require("./Controllers/AdminController")

const server = Hapi.server({
    routes: {
        cors: {
            origin: ["http://localhost:7061", "http://localhost:7062", "https://app.paulsessays.com", "https://app.paulsessays.com"],
            credentials: false
        },
    },
    state: {
        strictHeader: false
    },
    port: 7060,
});

var rule2 = new cron.RecurrenceRule();
rule2.dayOfWeek = [0,1,2,3,4,5,6];
rule2.hour = 23;
rule2.minute = 59;

const ValidateAdmin = async function (decoded, request) {
    var deferred = Q.defer();

    var extratedToken = request.headers.authorization.substring(7, request.headers.authorization.length);
    jsonwebtoken.verify(extratedToken, configuration.superadmin.secret, function(err, decoded) {
        if(decoded === undefined) {
            deferred.resolve({ isValid: false });
        }
        else {
            AdminController.findAdminByEmail(decoded.email)
            .then(person => {
                deferred.resolve({ isValid: true })
            })
            .catch(err => {
                deferred.resolve({ isValid: false });
            })
        }
    });
    
    return deferred.promise;
};

const ValidateSuperAdmin = async function (request, username, password, h) {
    var isValid = false
    var credentials = {}
    if(username === configuration.superadmin.username && password === configuration.superadmin.acesskey) {
        isValid = true
    }
    
    return { isValid, credentials };
};

const swaggerOptions = {
    info: {
        title: 'Pauls Essay API',
        version: Pack.version,
    },
};

const init = async () => {
    
    await server.register(require('inert'));

    /*await server.register({
        plugin: require('hapi-geo-locate')
    })*/
    /*await server.register({
        plugin: require('hapi-pino'),
        options: {
            prettyPrint: false,
            logEvents: ['response']
        }
    });*/

    await server.register(Vision)

    await server.register(require('hapi-auth-jwt2'));
    await server.register(require('hapi-auth-basic'));

    await server.register({
        plugin: HapiSwagger,
        options: swaggerOptions
    })

    await server.auth.strategy('admin', 'jwt', 
        { key: configuration.superadmin.secret,          
        validate: ValidateAdmin,           
        verifyOptions: { algorithms: [ 'HS256' ] } 
    });

    await server.auth.strategy('superadmin', 'basic', {validate: ValidateSuperAdmin});

    server.route(Routes);

    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
};

//server.ext('onPreResponse', corsHeaders)

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

cron.scheduleJob(rule2, function(){
    AdminController.ResetDailyCounter()
    .then(success => {
        console.log('daily visitor counter reseted')
    })
    .catch(error => {
        console.log(error)
    })
});

init();
