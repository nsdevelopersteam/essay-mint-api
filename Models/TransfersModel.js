var mongoose     = require('mongoose');
mongoose.set('useCreateIndex', true);
var Schema       = mongoose.Schema;

var TransferModel = new Schema({
    senderBatchId: {type: String, required: true},
    created: {type: Date, default: Date.now},
    total: {type: Number, required: true},
    currency: {type: String, required: true},
    transferId: {type: String, required: true},
});

module.exports = mongoose.model('transfer', TransferModel);