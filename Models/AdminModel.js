var mongoose     = require('mongoose');
mongoose.set('useCreateIndex', true);
var Schema       = mongoose.Schema;

var AdminModel = new Schema({
    name: { type: String, required: true },
    email: {type: String, required: true, unique: true},
    password: { type: String, required: true},
    publicId: {type: String, required: true},
    created: {type: Date, default: Date.now},
    authToken: {type: String},
    firstTime: {type: Boolean, default: true}
});

module.exports = mongoose.model('admin', AdminModel);