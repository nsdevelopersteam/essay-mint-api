var mongoose     = require('mongoose');
mongoose.set('useCreateIndex', true);
var Schema       = mongoose.Schema;

var PaymentHistoryModel = new Schema({
    publicId: {type: String, required: true},
    created: {type: Date, default: Date.now},
    paymentInfo: {
        cardNumberLast4: { type: String, required: true },
        cardExpDate: {type: String, required: true},
    },
    total: {type: Number, required: true},
    currency: {type: String, required: true},
    paymentId: {type: String, required: true},
    isUsed: {type: Boolean, required: true, default: false}
});

module.exports = mongoose.model('payment', PaymentHistoryModel);