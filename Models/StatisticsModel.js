var mongoose     = require('mongoose');
mongoose.set('useCreateIndex', true);
var Schema       = mongoose.Schema;

var StatisticModel = new Schema({
    publicId: {type: String, required: true},
    created: {type: Date, default: Date.now},
    numberOfVisits: {type: Number, required: true, default: 0},
    undepositedAmount: {type: Number, required: true, default: 0},
    totalEarned: {type: Number, required: true, default: 0},
    totalNumberOfOrders: {type: Number, required: true, default: 0},
    lastTransferDate: {type: String}
});

module.exports = mongoose.model('statistic', StatisticModel);