var mongoose     = require('mongoose');
mongoose.set('useCreateIndex', true);
var Schema       = mongoose.Schema;

var OrderModel = new Schema({
    publicId: {type: String, required: true},
    orderId: {type: Number, required: true}, // a 6 digits number that represents the order
    created: {type: Date, default: Date.now},
    status: {type: String, required: true, default: 'submitted'},
    type: {type: String, required: true}, // Type of essay
    title: {type: String, required: true}, // title of the essay
    numberOfPages: {type: Number, required: true},
    spacing: {type: String, required: true},
    academicLevel: {type: String, required: true},
    urgency: {type: String, required: true},
    style: {type: String, required: true},
    languageStyle: {type: String, required: true},
    numberOfSources: {type: String, required: true},
    notes: {type: String},
    sourcesList: [{
        path: {type: String},
    }],
    total: {type: Number, required: true},
    fee: {type: Number, required: true},
    paymentConfirmationId: {type: String, required: true},
    currentDownloadURL: {type: String},
    downloadLinkValidation: {type: Boolean, required: true, default: true},
    numberOfDownloads: {type: Number, required: true, default: 0},
    customer: {
        name: {type: String, required: true},
        email: {type: String, required: true},
    }
});

module.exports = mongoose.model('order', OrderModel);